
all: clean
	for i in content/posts/* content/pages/*; do\
	  if [ -d "$$i" -a -f "$$i/Makefile" ]; then\
	    (cd "$$i"; make);\
	  fi;\
	done
	hugo

pull:
	@(cd themes/versatile; git checkout master; git pull)
	@git pull

push:
	@(cd themes/versatile; echo pushing theme...; git push; echo done)
	@echo pushing blog...; git push; echo done

clean:
	rm -fr public
	mkdir public


