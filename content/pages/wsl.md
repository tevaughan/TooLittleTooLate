---
title: "WSL"
date: 2020-11-09T18:33:52-07:00
description: "Notes on my use of WSL."
image: ""
tags:
  - "browser"
  - "cygwin"
  - "edge"
  - "focus"
  - "mouse"
  - "windows-terminal"
  - "wsl"
  - "wsltty"
  - "x-windows"
---

WSL-2 provides a neat way to run Microsoft Windows 10, docker containers, and
multiple different distributions of Linux, all simultaneously and with very
little overhead in system-resource.

Aside from the fact that this can be done in a way that does not tax the
computer's resources, the best thing about WSL might be that it allows for
tight integration between applications in Windows and those in Linux, as each
can be invoked from the other side; standard IO works these days across
operating systems and shells; and all filesystems are reachable from
anywhere.

When I first wrote this page (2020 Nov), the biggest drawbacks seemed to be

- WSL's lack of direct use of the GPU (whether for display or for compute)
  and

- the need to use vpnkit to enable WSL's access to the network when a VPN is
  running in Windows.

By 2023, the first of these was no longer much of an issue, as WSLg became
available for Windows 10.

<!--more-->

## Overview

Back in 2020, for graphical Linux-applications, I ran Cygwin's X-server.

- The server works in single-window mode and/or multi-window mode.
  - I used only multi-window mode because I had configured MS Windows to
    focus a window when the mouse moves over it (no click required); see
    below.
- OpenGL worked best via LLVM-pipe, both under WSL-1 and under stock WSL-2.
  - This worked remarkably well unless vpnkit was being used to work around a
    VPN in Windows; then X still worked, but OpenGL-performance became much
    worse.

WSL allows running multiple Linux-distributions simultaneously with much less
resource than would be required to run multiple virtual machines
simultaneously.

- Every virtual machine needs its own reserved RAM, but RAM resources are
  shared in WSL.
- With WSL-1, there is no Linux kernel but a translation-layer for
  system-calls.
- With WSL-2, all running distributions (and docker-containers) share the
  same instance of the same kernel.
- WSL does need a separate file-storage for each distribution, but storage is
  relatively inexpensive compared to other resources.

WSL-2 has some advantages and disadvantages relative to WSL-1, but WSL-2 is
generally better than WSL-1.  WSLg (with WSL-2) adds accelerated graphics via
the GPU.

### Preparation for Installation

For those stuck at an old revision of Windows (1903 or 1909), there are
[backports available][bp1].
  - An [update][bp2] is required for that.
  - The update can be downloaded [here][bp3].

[bp1]: https://devblogs.microsoft.com/commandline/wsl-2-support-is-coming-to-windows-10-versions-1903-and-1909/
[bp2]: https://www.bleepingcomputer.com/news/microsoft/windows-10-kb4566116-update-fixes-crashing-settings-unlock-bug/
[bp3]: https://www.catalog.update.microsoft.com/Search.aspx?q=KB4566116

### Installation

One can

- install a Linux-distribution from the Microsoft Store (as I did for Debian)
  or

- [manually install a Linux distribution][nostore] (as I did for Ubuntu-18.04
  and Ubuntu-20.04).

[nostore]: https://docs.microsoft.com/en-us/windows/wsl/install-manual

### Work-Around for VPN

WSL-2 can lose the ability to connect to internet when the host is running a
VPN like GlobalProtect.
  - [See page on work-around.](/pages/wsl2-vpn.html)


## Basic Configuration of Shell

In my setup, my user-account in each distribution is configured so that:

- `~/.bashrc` is logical link pointing to `/etc/skel/.bashrc`.
- `/etc/skel/.bashrc` runs [~/.bash_aliases][10], my generic
  BASH-configuration.
- `~/.bash_aliases` runs [~/.config/bash_aliases][cba], my BASH-configuration
  specific to a WSL-machine.

[10]: https://gitlab.com/tevaughan/config/-/blob/master/.bash_aliases
[cba]: https://gitlab.com/tevaughan/config/-/blob/master/.config/bash_aliases-wsl


## PATH

The appropriately translated version of the Win32 `PATH` environment-variable
is by default appended to the Linux `PATH` environment-variable by WSL when
Linux is launched.

However, this was being overridden by the definition of `PATH` in
`/etc/profile`.

I edited `/etc/profile` so that it appends the previous value of `PATH` to
the newly defined value.

Now all is good.


## Terminal

- Default terminal for WSL leaves some things to be desired.
  - I don't much like the functioning of the history-buffer.
  - There are some odd issues with the colors when in vim.

- [wsltty][2] seems better.
  - It seems to be based on [mintty][mintty] that comes with Cygwin.
  - It supports, right in the terminal,
    - [sixel-images][sixel],
    - [base64-images][base64], and
    - [vector graphics][vector].
  - For each distribution installed, I have made a shortcut to start `wsltty`.
    - For example, the shortcut that starts a shell in Ubunut-18.04 has the
      following in its "Target:"-field.
      ```
      C:\Users\<UserName>\AppData\Local\wsltty\bin\mintty.exe --WSL="Ubuntu-18.04" --configdir="C:\Users\<UserName>\AppData\Roaming\wsltty"  -
      ```
  - By default the shortcut was set to start up in
    ```
    c:\Users\<UserName>
    ```
  - That caused some annoyance because then `bash` did not begin in my WSL
    home-directory.
  - So I set the shortcut's `Start in:` field to be
    ```
    \\wsl$\Debian\home\<UserName>
    ```
    - Here I am suggesting that `<UserName>` is the same in Linux as in
      Windows, but this need not be so.

[2]: https://github.com/mintty/wsltty
[mintty]: https://mintty.github.io/
[sixel]: https://mintty.github.io/#sixel
[base64]: https://mintty.github.io/#image
[vector]: https://mintty.github.io/#tek

- [Windows Terminal][winterm] also seems better.
  - It uses OpenGL to render and scroll fast.
  - It supports multiple tabs.
  - It is easily integrated with WSL.
  - The only real drawbacks that I've found so far:
    - The icon is always the same, unlike wsltty, whose icon becomes that of
      the shortcut that launches it.
    - The `resize`-command does not work from the shell in the window.

[winterm]: https://github.com/microsoft/terminal

## Focus Follows Mouse

The tightest integration is available with multi-window mode for X-windows
and when native-Windows-GUI apps are used wherever possible.

I prefer focus-follow-mouse mode, though.

There was a [solution](/posts/focus-follows-mouse-2020-oct-14.html), but that
was no longer working form me as of 2023.

[3]: https://gojimmypi.blogspot.com/2019/01/wsl-gui-using-cygwin-x-server-for.html
[4]: https://superuser.com/questions/1180005/cygwin-x-and-windows-subsystem-for-linux

