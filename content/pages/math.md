---
title: "Math is Beautiful"
date: 2020-07-15T09:19:09-06:00
description: "It should look beautiful, too."
tags: ["blog", "math"]
---

Among my requirements for putting any time into a blog are

- for the blog to afford me the opportunity to write mathematical content,
  whenever I feel the need, and

- for the blog to make that content look good.

For example,
$$
\int u \\; \mathrm{d}v = uv - \int v \\; \mathrm{d}u.
$$

<!--more-->

I shall not likely be able to put *substantial* mathematical content into the
blog because web-rendered mathematical text seems always inferior to that
produced via [LaTeX][2].  But I should like to have the power to write the
occasional math-heavy post.

The theme, [beautifulhugo][0], that I use for this blog allows me to write
according to the rules of [KaTeX][1], which is similar to LaTeX.

## In-Line Math

KaTeX renders `\\(c^2 = a^2 \\; b^2\\)`, as "\\(c^2 = a^2 \\; b^2\\)."

## Display-Mode Math

KaTeX renders
```
$$
f(x) = \int_{-\infty}^\infty g(u) \\: e^{2 \pi i u x} \\, \mathrm{d}u.
$$
```
as
$$
f(x) = \int_{-\infty}^\infty g(u) \\: e^{2 \pi i u x} \\, \mathrm{d}u.
$$

## Characters Conflicting With Markdown

Certain characters are rendered by markdown, and each needs a work-around.  For
example, the `'` character can  be replaced with `^\prime`:

`G^\prime = G - u` is rendered as "\\(G^\prime = G - u\\)."

The `"` character can  be replaced with `^{\prime\prime}`:

`G^{\prime\prime} = G^\prime - v` is rendered as "\\(G^{\prime\prime} =
G^\prime - v\\)."


[0]: https://github.com/halogenica/beautifulhugo
[1]: https://github.com/Khan/KaTeX
[2]: https://www.latex-project.org/
