---
title: What in the world *is* this, anyway?
date: 2020-07-15
description: I'm not sure myself.
tags: ["blog", "history"]
---

This site is Thomas E. Vaughan's blog. I like to write about
- free software,
- math,
- physics,
- Aristotelian metaphysics,
- the philosophy of science, and
- whatever else strikes me.

<!--more-->

## Who I Am

- I have been married since the end of 1991.
- I have worn a beard continuously since the beginning of 1992.
- Linux has been my main operating system since the beginning of 1993.
- My wife and I have eight children.
- Yes, we are trying to be Catholic.


## What To Call It

When I began this blog, I was well past the half-way point of my expected
life-span.

### Too Little, Too Late

Even if I manage to keep up the discipline of a journal long enough to produce
something worth looking at, I suspect that the effort will be too little and
too late to be of much value subjectively, let alone objectively.  As Maverick
Philosopher has noted, a journal written in youth is a treasure in old age.
But I did not write a journal in my youth.  Perhaps beginning now is too late
to make a journal of much use, but who knows?

### But Maybe Something Is Better Than Nothing

As bodily decay sets in, I am spurred to reform my life.  One reform is to
start writing regularly, and *not* on Facebook.  Leaving some curated ideas for
myself, later in life, and for others, perhaps later still, seems better than
not leaving any.


## Bits of History

### Some Early Happenings

I lived in San Antonio, Texas, for the 12 years of public school. While there,
I did many stupid things of which I am embarrassed.  This period passed in two
phases.  At first, I lived to the northwest in a suburban town called Leon
Valley.  In the Summer after seventh grade, my family moved far enough away to
the north and east as to make my keeping up relationships with my old friends
impractical.  To some degree, I was relieved because I could start fresh, and
none of my new friends would know about the stupidities that I had been guilty
of among my old friends.  But of course one brings one's stupidities wherever
one goes, and they can grow fresh embarrassments in new soil.

This period straddled the 1970s and 1980s; so although I was nominally
Catholic, I had almost no formal catechesis whatsoever.  In this period,
shortly after the Second Vatican Council, the Catholic Church in the United
States seems in many a place to have simply stopped teaching children the
doctrine of the Faith, or at least to have watered it down so much as not to
give children any clear conception of what the Church actually teaches.  Of
course, the family is the proper place for primary religious instruction, but
my parents did not perceive the need for any organized approach.  Ignorance of
the faith among cultural Catholics of my generation seems to be a primary
reason for one to drift away as I did.

During this period, I did well enough at gymnastics to get an athletic
scholarship to the University of Oklahoma (OU).

### Some Middle Happenings

I was enrolled as a student at OU for 15 years or so.  While there, I did many
stupid things of which I am embarrassed.  Also, I studied astrophysics both as
an undergraduate and as a graduate-student.

After the undergraduate-part, a woman picked me to be her husband.  It is still
hard for me to believe that I was able to escape what was for me the torture of
not being married.  My only real contribution to the escape seems to have been
the presence of mind not to screw up the opportunity that she offered me.  I
took it (and her).  We should eventually raise eight children.

I am not sure how I finished the Ph.D. in physics, but it seems likely that my
professors both felt sorry for me and wanted me to leave.  Perhaps they
rationalized formalizing for me a departure better than what I deserved.  I am
thankful to them, especially to [David Branch][db].

In the late undergraduate period and especially after I was married, the
admitted agnosticism, practical atheism, and philosophical materialism that I
had drifted into started slowly, over years, to become less and less attractive
to me.  The process accelerated, shortly before I (at long last) finished
graduate school, when my wife, who had converted to Catholicism when we were
married, made me read [What Catholics Really Believe][0] by Karl Keating.  I
followed this up quickly by reading [Catholicism and Fundamentalism][0a] (also
by Keating) and then [An Essay on the Development of Christian Doctrine][0b] by
John Henry Newman.

Not long after leaving OU, I found a job as a software-engineer at Ball
Aerospace in Boulder.  I've been there since about 2001.  Since then, I have
done many stupid things of which I am embarrassed, mostly in words that I have
posted to Facebook.

### Some Late Happenings

After 2001, I became a political conservative and also, along the way,
disabused myself of the last vestiges of the philosophical materialism that had
been hanging around in my mind.  Both of these changes follow naturally from an
intellectual approach to Catholicism, but neither of them requires Catholicism.
A good, general summary of why one should embrace traditional morality and
eschew atheistic materialism can be found in [The Last Superstition][f1] by
Edward Feser.  Feser happens to be Catholic, but the book is just about
philosophy, not about the Faith.


## How I Made This Site

This website is powered by [GitLab Pages][1] and [Hugo][2].  It uses my
[versatile][v] theme.

[My blog-project][mp] is a customised fork of [GitLab's Hugo-template-project][3].

[db]: https://www.nhn.ou.edu/people/branch
[0]: https://www.amazon.com/What-Catholics-Really-Believe-Misconceptions/dp/0898705533
[0a]: https://www.amazon.com/Catholicism-Fundamentalism-Attack-Romanism-Christians/dp/0898701775/
[0b]: https://www.amazon.com/Essay-Development-Christian-Doctrine-Notre/dp/026800921X
[f1]: https://www.amazon.com/Last-Superstition-Refutation-New-Atheism/dp/1587314525
[1]: https://about.gitlab.com/features/pages/
[2]: https://gohugo.io
[3]: https://gitlab.com/pages/hugo
[v]: https://gitlab.com/tevaughan/versatile
[mp]: https://gitlab.com/tevaughan/TooLittleTooLate

