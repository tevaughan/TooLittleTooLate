---
title: "New Debian Packages for 2020 Oct 27"
date: 2020-10-27T08:36:39-06:00
description: "New packages of interest in sid."
image: ""
tags:
  - "debian"
  - "package"
  - "tty-share"
---

New Debian-packages for today include
  - `tty-share`

<!--more-->

## tty-share

tty-share is a very simple command line tool that gives remote access to a UNIX
terminal session.

tty-share is used on the machine that wants to share the terminal, and it
connects to the server to generate a secret URL, over which the terminal can be
viewed in the browser.

tty-share connects over a TLS connection to the server (runs at tty-share.com),
which uses a proxy for the SSL termination, and the browser terminal is served
over HTTPS. The communication on both sides is encrypted and secured. However,
end-to-end encryption is still desired, so nothing but the sender and receiver
can decrypt the data passed around.

Homepage: https://tty-share.com


