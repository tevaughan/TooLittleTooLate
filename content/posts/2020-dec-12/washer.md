---
title: "I Fixed My Broken Washing Machine..."
date: 2020-12-12T12:07:06-07:00
description: "... by blowing hard into a hose."
image: "washer-cropped.jpg"
tags:
  - "front-loading"
  - "samsung"
  - "washer"
  - "washing-machine"
  - "water-level"
  - "sensor"
---

I have a front-loading washing machine (WF328AAW/XAA, circa 2007) made by
Samsung.  Recently it would fail and display the error-code, `nd`.  After my
wife and I
- opened up the machine,
- ran built-in diagnostic tests,
- used a multi-meter to probe things,
- ordered a replacement "water-level-sensor" part (only about $20), and
- installed the part,

the machine then would start to work on a load for a while but eventually fail
with a different error-code, `nF`.

In the end, I fixed the failure by disconnecting the pneumatic hose from the
newly replaced part (which probably did not need to be replaced) and then by
blowing into the exposed end of the hose to remove an invisible blockage inside
the drum.

The story of how I guessed this fix is a story of how modern science works, a
story of how a scientific theory can be wrong but still useful, a story of
reasoning about something unseen.

<!--more-->

## First Problem

The washer has not been my wife's favorite device over the past decade and
more, but at least it has not broken down until now.  (You would have to ask my
wife for details about what she does not like about its operation.)
<center>
<a href="/posts/2020-dec-12/washer.jpg">
<img src="/posts/2020-dec-12/washer.jpg" width="300"/>
</a>
<p><i>WF328AAW/XAA, built circa 2007.</i></p>
</center>

The initial failure coincided with the display of `nd` on the LED-panel.
According to the document, entitled "SAMSUNG WASHING MACHINE Technical
Information,"
<center>
<a href="/posts/2020-dec-12/tech-info.jpg">
<img src="/posts/2020-dec-12/tech-info.jpg" width="300"/>
</a>
<p><i>The document that I found in a plastic bag taped to the machine.</i></p>
</center>

the code `nd` is Diagnostic Code 1, whose description is, "The water level
fails to drop below the Reset Water Level within 15 minutes."

<center>
<a href="/posts/2020-dec-12/table.jpg">
<img src="/posts/2020-dec-12/table.jpg" width="300"/>
</a>
<p><i>Top of the table on the reverse side of the document's
title-page.</i></p>
</center>

`nd` seems then to indicate that there is a problem with the sensing of the
level of the water in the drum.  Because, in our case, no water would come into
the dry machine when it was asked to wash a load of laundry, the problem seemed
to be that the water-level sensor was indicating a full drum of water even when
there was no water.

## Initial Investigation and First Hypothesis

My wife was interested in doing some investigation before we moved forward with
ordering a part. While I was at work, she removed the drain-pump and cleaned
it. When I got home, I used my multi-meter to measure the resistance across the
coils of the drain-pump's motor, and I found that the resistance was right in
the middle of its specified range. She did some general cleaning, to make sure
that filters on the input-hoses were free of debris, etc., and she reinstalled
the drain-pump.

Later that night, I used the Technical-Information document to figure out how
to turn on the machine in Quick-Test Mode.
<center>
<a href="/posts/2020-dec-12/quick-test.jpg">
<img src="/posts/2020-dec-12/quick-test.jpg" width="300"/>
</a>
<p><i>How to enter and to use Quick-Test Mode. Note that the instructions have
an error, which I have marked out with pencil. The machine needs to be off when
the Spin-Key, the Silver-Care Key, and the Power-Key are pressed
simultaneously.</i></p>
</center>

This allowed me to verify that each of the hot and cold supplies could be
routed through valves into the drum in every combination through the bleach-
and soap-dispensers. I verified that the drum would spin. I verified that the
drain-pump would remove the water from the drum. I verified that the door would
lock and unlock.

But eventually it seemed to me that we should order a new "water-level sensor."
I am using scare-quotes because what we ordered is not the whole water-level
sensor but a pneumoelectric sensor, which is a part of the water-level sensor.
Other parts of the water-level sensor are
 - a pneumatic tube that connects the pneumoelectric sensor to the side of the
   drum and
 - (invisible to me) whatever part of the sensing mechanism lies within the
   drum on the other side of the port.

What I'm calling a "pneumoelectric sensor" is what converts the pneumatic
hose's pressure into an electrical signal.

According to the first hypothesis, the pneumoelectric sensor had failed.
Because it seemed always to be indicating a full drum of water, even when no
water was in the drum, the sensor needed to be replaced.

## Second Problem and Second Hypothesis

The new part arrived while I was at work, and my wife installed it.
<center>
<a href="/posts/2020-dec-12/transducer.jpg">
<img src="/posts/2020-dec-12/transducer.jpg" width="300"/>
</a>
<p><i>My wife holds the new pneumoelectric sensor next to the old one.</i></p>
</center>

After she replaced the pneumoelectric sensor and tried to start a load, the
machine would progress further when commanded to wash a load of laundry.  Water
would now come into the drum.  The drum would start to spin (on and off).  But
there would eventually appear a different error-code, `nF`, and the machine
would stop working.  The new code is Diagnostic Code 3, whose description is,
"When the filling continues for more than 16 minutes, or there is no change of
water-level for three minutes."

Though different from `nd`, `nF` still seems to indicate that there is a
problem with the sensing of the level of the water in the drum.  However, after
the part was replaced, the problem seemed to be the *opposite* of the original
problem.
- Before the replacement, the machine seemed to decide that the drum was always
  *full* of water (even when no water had entered the drum).
- After the replacement, the machine seemed to decide that the drum was always
  *empty* of water (even when it had been filling for three minutes).

On the evening after this discovery, I started to think that the problem would
become very expensive.  However, while I was lying in bed in the night, I
thought of a simple, new hypothesis to explain all of the observations, and
this second hypothesis led me to think of an experiment that might result in a
fix.  What led me to this second hypothesis was my deep skepticism of the
apparent malfunctioning of *both* the old pneumoelectric sensor *and* the new.

## Resolution and Final Hypothesis

According to my second hypothesis, something in the invisible portion of the
water-level sensor, inside the drum, was rendering the pneumoelectric sensor,
outside the drum, incapable of registering a change in water-level.  Not having
seen the part inside the drum, I wondered whether there was perhaps a stiff
membrane that, as the roof of a sealed chamber filled with air, could pop
between two states, one convex and one concave.  If the chamber were near the
bottom of the drum, then, when the drum filled with water, the pressure of the
column of water above the membrane would pop it down into the concave state,
with lower volume inside an air-filled chamber connected through a port in the
drum to the pneumatic hose.  The membrane's popping down would produce a change
of pressure in the chamber and in the hose.  Also according to my hypothesis,
the membrane might have become stuck in the concave state, and that would
explain all of the observations, both of the code `nd` and, later on, the code
`nF`.

As to the code `nd`: After the invisible part of the water-level sensor became
stuck, and while the initial pneumoelectric sensor was still connected, the air
in the pneumatic hose was always at relatively high pressure, and this would
cause the computer always to decide that the drum was full of water even when
the drum was not full.

As to the code `nF`: According to the second hypothesis, when my wife replaced
the pneumoelectric sensor, she disconnected the pneumatic hose, and this
allowed the pressure in the hose to reach equilibrium with the pressure of the
air in the room.  Because the part of the sensor inside the drum was still
stuck, though, when she connected the hose to the new sensor, it would always
register low pressure, or no water in the drum, no matter how much water might
have come into the drum.

With these thoughts in mind, when I arose in the morning, I went immediately to
the laundry-room.  I disconnected the hose from the pneumoelectric sensor.  My
intention was to blow with increasing pressure into the hose until I might hear
a click indicating that I had popped the membrane back up into its convex
state.  As I increased the effort to push air into the hose, I eventually
reached the point of needing to exert some real effort.  Then there was a
little sound, but I was then able immediately and easily to blow air through
the hose as a continuous stream.

Either I had broken the sealed chamber, or, *what suddenly seemed more likely,
there was never any sealed chamber to beging with*!  Most likely, the system
had been simpler than I had imagined: Water probably just migrated into and out
from the tube as the pressure (due to the weight of the drum's water from the
surface down to the port where the hose attached) changed with the height of
the water's surface.

According to my third and final hypothesis, my blowing seems to have cleared a
blockage, perhaps some small bit of debris or encrustation, near the entrance
to the tube.

Such a blockage could have caused all of the symptoms: If the blockage had
formed when the water-level was high, then the system would behave just as it
would have according to my second hypothesis.

So, although my second hypothesis about some details of the mechanism seems to
have been inaccurate, the hypothesis was nevertheless good enough to produce a
solution to the problem.

Clearing the blockage by blowing hard made all problems disappear, and the
machine works normally again!

