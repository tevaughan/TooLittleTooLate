---
title: "New Debian Packages"
date: 2021-01-25T16:21:30-07:00
description: "New packages of interest to me."
image: ""
tags: ["debian", "package", "zmk"]
---

- `zmk`

<!--more-->

*collection of reusable Makefiles*

Collection of makefiles implementing a system similar to autotools, but without
the generated files that make understanding system behaviour harder.

Highlights include:

* Describe programs, test programs, static libraries, shared libraries,
  development headers, manual pages and more
* Use familiar targets like "all", "check", "install" and "clean"
* Works out of the box on popular distributions of Linux and MacOS
* Friendly to distribution packaging expecting autotools
* Compile natively with gcc, clang, tcc or the open-watcom compilers
* Cross compile with gcc and open-watcom
* Efficient and incremental, including the install target

Homepage: https://github.com/zyga/zmk
