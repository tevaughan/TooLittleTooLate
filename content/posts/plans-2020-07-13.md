---
title: Plans Are Worthless
description: Planning is everything.
date: 2020-07-13
tags: ["org-mode", "blog", "math"]
---

I have heard the titular expression attributed to President Eisenhower.
Here is what I plan to do with the blog.

<!--more-->

- I should like, some day, to begin organizing something via org-mode in emacs.
- Eventually, I'd like to write blog-entries here via org-mode.
- Yes, that's a thing, and it seems a good thing.
- But one thing at a time.
- At first, write at least one small entry per day in the blog.
- Reflect on computing, philosophy, or whatever strikes me on a given day.

I am using some key features today:

1. *description*.  The front-matter in YAML at the top of the source-file for
   the present entry contains not just the date and the title but also the
   *description*.

2. *tags*.  The front-matter for the present entry contains also a list of
   *tags*.

3. *math*.  [KaTeX][1] renders inline `\\(c^2 = a^2 \\; b^2\\)` as \\(c^2 = a^2
   \\; b^2\\) and in display-mode:
```
$$
f(x) = \int_{-\infty}^\infty g(u) \\: e^{2 \pi i u x} \\, \mathrm{d}u.
$$
```

$$
f(x) = \int_{-\infty}^\infty g(u) \\: e^{2 \pi i u x} \\, \mathrm{d}u.
$$

[1]: https://github.com/Khan/KaTeX
