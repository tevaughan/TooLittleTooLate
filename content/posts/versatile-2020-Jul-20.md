---
title: "Versatile"
date: 2020-07-20T18:22:38-06:00
description: "A theme designed for a filesystem-based notebook or a blog."
tags: ["blog", "notebook"]
---

I have ported the blog to my new theme
[versatile](https://gitlab.com/tevaughan/versatile).

<!--more-->

I use `versatile` at work to generate my engineering workbook. The workbook is
deployed to a file-server and accessed without a web-server, just by pointing
the browser at `index.html` at the top of the deployed tree.

But math is beautifully rendered by KaTeX, even when the internet is down!

Anyway, `versatile` is versatile enough so that it works also for my blog here
on github-pages.  The opengraph metatags are provided for pasting a link from
the blog into a social medium such as Facebook.

My hopes are

- that the vast majority of the tweaks to the appearance of the site are behind
  me and therefore

- that I can start to work on content, especially content unrelated to
  technical description of the blog itself.

