---
title: "New Debian Packages"
date: 2021-01-08T09:57:12-07:00
description: "New packages that I noticed in Sid on 2021 Jan 08."
image: ""
tags:
  - "catch"
  - "doxygen"
  - "doxygen2man"
  - "libcatch2-dev"
  - "socklog"
  - "socklog-run"
---

Since last time, these are the new packages that look interesting to me:
  - `socklog-run`
  - `doxygen2man`
  - `libcatch2-dev`

`socklog`'s support for rotation based on file-size, which obviates the need
for log-rotating cron-jobs, caught my eye.

<!--more-->

## socklog-run

*system and kernel logging services - runit services*

`socklog` cooperates with the `runit` package to create a small and secure
replacement for `rsyslog`. socklog supports system logging through Unix domain
sockets (`/dev/log`), UDP sockets (0.0.0.0:514), as well as TCP socket, with
the help of `runit`'s `runsvdir`, `runsv`, and `svlogd`. `socklog` provides a
different network logging concept, and also does log event notification.
`svlogd` has built in log file rotation based on file size, so there is no need
for any cron jobs to rotate the logs. `socklog` is small, secure, and reliable.

This package includes the configuration files to run `socklog` under `runit`
supervision as system and kernel log daemons. The `socklog-unix` and
`socklog-klog` services are enabled by default. The `socklog-inet`,
`socklog-notify`, and `socklog-ucspi-tcp` services are examples and disabled by
default.

Homepage: http://smarden.org/socklog

## doxygen2man

*generate man pages from Doxygen XML files*

This is a tool to generate API manpages from a doxygen-annotated header files.
First run doxygen on the header files and then run this program against the
main XML file it created and the directory containing the ancilliary files. It
will then output a lot of \*.3 man page files which you can then ship with
your library.

Homepage: https://github.com/clusterlabs/libqb/wiki

## libcatch2-dev

*C++ Automated Test Cases in Headers - static library*

Catch2 is a multi-paradigm test framework for C++, which also supports
Objective-C (and maybe C). It is primarily distributed as a single header file,
although certain extensions may require additional headers.

If you've been using an earlier version of Catch, please see the Breaking
Changes section of the release notes before moving to Catch2.

This package provides the static library Catch2WithMain. It should provide a
shared impl for all targets that need to link against Catch2's implementation.
However, due to limitations of C++ linking and Catch2's v2 implementation, this
is only experimental and might not work under some circumstances.

Homepage: https://github.com/catchorg/Catch2

