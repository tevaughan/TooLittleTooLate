---
title: "Getting Versatile Ready"
date: 2020-07-22T21:10:38-06:00
description: "I wonder if it will ever really be ready...."
tags: ["hugo", "blog", "uglyurls", "config.toml"]
---

Getting [versatile][1] ready for public release requires some work.

[1]: https://gitlab.com/tevaughan/versatile

<!--more-->

I need to make the theme work regardless of whether `uglyURLs` be set in the
site's `config.toml`.

One fix is to make `partials/pagination.html` respect the uglyURLs setting.

