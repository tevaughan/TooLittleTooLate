---
title: "Organizing Distributions in WSL"
date: 2020-12-20T16:00:51-07:00
description: "I spent this weekend on an effort mostly cosmetic."
image: "screen-cropped.jpg"
tags: ["wsl", "organization", "icons", "beauty"]
---

Sometimes one needs to organize one's environment in order to make it beautiful
and efficient.  Both beauty and ease of use reduce the stress of getting
ordinary work done.  Toward that end, I have

- [made][3] some [beautiful icons][0] for WSL-related things in Windows 10 and

- [modified my shell prompt][1] to tell which distribution the shell is running
  in.

Each of these things make it easy to tell which Linux-distribution a particular
window belongs to.

[0]: https://gitlab.com/tevaughan/config/-/tree/master/Pictures/icons
[1]: https://gitlab.com/tevaughan/config/-/blob/master/.bash_aliases
[3]: https://gitlab.com/tevaughan/config/-/blob/master/Bin/make-icon

(See my [WSL-page](/pages/wsl.html) for full documentation on how I configure
WSL, for tips, for tricks, etc.)

<!--more-->

![screen](screen.png)

## The Problem

WSL makes it very easy to have several different Linux-distributions running
simultaneously on a Windows-10 machine.  It is easy because every distribution
is running against the same, single instance of the Linux-kernel. So memory-
and processor-resources are efficiently shared with almost no overhead per
distribution, and the only penalty of adding another distribution is disk-space
for the distribution's files.

I have Debian Buster, Ubuntu 18.04, and Ubuntu 20.04 all installed, and I often
have one or two terminal-windows from each machine open simultaneously.

The problem was that I could easily become confused about which window was
attached to which distribution.

## The Solution in Summary

I launch my [terminal-program][wsltty], by way of a Windows-shortcut. I have a
different shortcut for each distribution.

I discovered that if I change the shortcut's icon, then the terminal-program
shows up with that icon in the upper left corner of its window, and that icon
is also used to differentiate it on the task-bar.

For added clarity, I also [modified my shell-prompt][1] to print the contents
of `/etc/issue.net` on the line containing the current path.  `/etc/issue.net`
has the name and version of the distribution, at least in a Debian-derived
system.

[wsltty]: https://github.com/mintty/wsltty

## Making a Beautiful Set of Icons

I [discovered][stack] at StackOverflow how to use [ImageMagick][2]'s
`convert`-command to generate a nice icon-file for Microsoft Windows.
  - The trick is independently to scale a high-resolution image down to each of
    several standard icon-sizes, and to store every scaled icon in the same
    icon-file.
  - Then Windows picks the appropriate image to render for each context.
  - Without doing this, the icons are automatically scaled by Windows and end
    up looking ugly in most contexts.
  - I wrote a little [shell-script][3] to do the job right.

[2]: https://imagemagick.org/index.php
[stack]: https://stackoverflow.com/questions/11423711/recipe-for-creating-windows-ico-files-with-imagemagick

## Putting Them To Use

I put my nice icons to use
- not just in differentiating the terminal-instances in a distribution-specific
  way
- but also in differentiating shortcuts to home-directories.

In fact, I'm continuing to find other uses for icon-based organization as I
write this.

