---
title: "Philosophies of Nature"
date: 2020-12-08T22:03:57-07:00
description: "Notes on Chapter 1 of Aristotle's Revenge"
image: ""
tags: ["feser", "nature", "philosophy"]
---

1. What the philosophy of nature is.
2. Aristotelian philosophy of nature.
3. Mechanical world picture.

<!--more-->

## Philosophy of Nature

Feser proposes that the philosophy of nature is a middle discipline between
natural science and metaphysics.

### Distinctions Between Natural Science and Metaphysics

On contingency:
- Natural science is concerned only with the contingent reality of how the
  physical universe happens to be.
- Metaphysics is concerned with what could possibly be, what must necessarily
  be, and what cannot possibly be.

On the material and the empirical:
- Natural science is concerned only with the material world and empirical
  investigation.
- Metaphysics is not so limited in its considerations, which include immaterial
  realities.

On principles:
- Natural science presupposes concepts that are taken for granted, but
  metaphysics studies and provides those concepts.
- Natural science presupposes the existence of substances, but metaphysics
  determines what it would mean for something to *be* a substance in general.
- Natural science presupposes causation in the material world, but metaphysics
  determines what can count as a cause in general.
- Natural science identifies laws of nature, but metaphysics determines what it
  is for something the be a law of nature in general.

### Where the Philosophy of Nature Fits

On contingency:
- Like metaphysics, the philosophy of nature is concerned with any possible
  material and empirical reality of how the physical universe might be.
- So the philosophy of nature is, in one way, like metaphysics, more general
  than natural science.

On the material and the empirical:
- Like natural science, the philosophy of nature is concerned only with the
  material world and empirical investigation.
- So the philosophy of nature is, in another way, like natural science, more
  specific than metaphysics.

