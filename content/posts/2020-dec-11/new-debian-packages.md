---
title: "New Debian Packages"
date: 2020-12-11T11:46:33-07:00
description: "New packages in Debian Unstable."
image: ""
tags:
  - "debian"
  - "diffoscope"
  - "inkscape-textext"
  - "makefile2graph"
  - "mkdepend"
  - "package"
  - "topydo"
  - "whitakers-words"
---

- diffoscope-minimal
- inkscape-textext-doc
- makefile2graph
- mkdepend
- topydo
- whitakers-words

<!--more-->

## diffoscope-minimal

`diffoscope` is a visual diff tool that attempts try to get to the bottom of
what makes files or directories actually different.

It can recursively unpack archives of many kinds, transforming various binary
formats into more human-readable form to compare them in a human-readable way.
It can compare two tarballs, ISO images or PDFs just as easily. The differences
can be displayed on the console or in a HTML report.

File formats supported include: Android APK files, Android boot images, Apple
Xcode mobile provisioning files, ar(1) archives, ASM Function, Berkeley DB
database files, bzip2 archives, character/block devices, ColorSync colour
profiles (.icc), Coreboot CBFS filesystem images, cpio archives, Dalvik .dex
files, Debian .buildinfo files, Debian .changes files, Debian source packages
(.dsc), Device Tree Compiler blob files, directories, ELF binaries,
ext2/ext3/ext4/btrfs/fat filesystems, FreeDesktop Fontconfig cache files,
FreePascal files (.ppu), Gettext message catalogues, GHC Haskell .hi files, GIF
image files, Git repositories, GNU R database files (.rdb), GNU R Rscript files
(.rds), Gnumeric spreadsheets, GPG keybox databases, Gzipped files,
Hierarchical Data Format database, ISO 9660 CD images, Java .class files, Java
jmod modules, JavaScript files, JPEG images, JSON files, LLVM IR bitcode files,
LZ4 compressed files, MacOS binaries, Microsoft Windows icon files, Microsoft
Word .docx files, Mono 'Portable Executable' files, Mozilla-optimized ZIP
archives, Multimedia metadata, OCaml interface files, Ogg Vorbis audio files,
OpenOffice .odt files, OpenSSH public keys, OpenWRT package archives (.ipk),
PDF documents, PE32 files, PGP signatures, PGP signed/encrypted messages, PNG
images, PostScript documents, Public Key Cryptography Standards (PKCS) files
(version #7), RPM archives, Rust object files (.deflate), SQLite databases,
SquashFS filesystems, symlinks, tape archives (.tar), tcpdump capture files
(.pcap), text files, TrueType font files, U-Boot legacy image files,
WebAssembly binary module, XML binary schemas (.xsb), XML files, XZ compressed
files, ZIP archives and Zstandard compressed files.

This -minimal package only recommends a partial set of the supported 3rd party
tools needed to produce file-format-specific comparisons, excluding those that
are considered too large or niche for general use.

diffoscope is developed as part of the Reproducible Builds project.

Homepage: https://diffoscope.org

## inkscape-textext-doc

TexText is a Python plugin for the vector graphics editor Inkscape providing
the possibility to add and re-edit LaTeX generated SVG elements to your
drawing.

Key features
- Windows/Linux/MacOS support
- LaTeX generated SVG elements can be re-edited later
- Multi-line editor with syntax highlighting
- Compilation with PdfLaTeX, XeLaTeX or LuaLaTex
- Interoperable scaling in TexText and Inkscape
- Font size match with Inkscape text
- Customizable TeX preamble (additional packages, parskip, parindent, etc.)
- Colorization via TeX commands/Inkscape is kept after re-editing
- Alignment anchor of the produced output
- Preview images

This package contains the documentation.

Homepage: https://textext.github.io/textext/

## whitakers-words

Whitakers Words is a program that takes in words in Latin, and analyzes them to
determine the stems, case, form,  and any possible  translations.  It can also
provide Latin words with a given translation, going from Latin to English.  It
is an invaluable tool for Latin users, be they experts or new learners.

Words was created by Willam Whitaker (1936-2010) as a curiosity that
demonstrated his new programming language, Ada.  It was since improved and
iterated on, ported away from it's DOS roots, and given an online access point.

This package contains the executable parser program.

Homepage: http://mk270.github.io/whitakers-words/

## makefile2graph

Output is a graphviz-dot file, a Gexf-XML file or a list of the deepest
independent targets that should be made. Sub-makefiles are not supported.

Homepage: https://github.com/lindenb/makefile2graph

## mkdepend

Package contains dependency generators for LaTeX, COM and Java source files.

Homepage: https://projects.ibt.lt/repositories/projects/mkdepend

## topydo

The Todo.txt project defines a format for supporting context-rich tasking in a
plain text file. It supports an ecosystem of tools which allow working with
tasks across hosts and operating systems.

This package installs a more advanced version of the command-line tool for
working with todo.txt tasking files.

Homepage: https://github.com/topydo/topydo

