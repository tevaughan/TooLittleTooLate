---
title: "Command Not Found"
date: 2020-12-11T10:13:07-07:00
description: "Debian has command-not-found package, not installed by default."
image: ""
tags: ["command-not-found", "debian", "ubuntu"]
---

On Ubuntu, the `command-not-found` package is installed by default.

On Debian, one must explicitly install it and then, as root, run
`update-command-not-found` before the package will work.

<!--more-->

This cool package hooks a utility into Debian's `/etc/bash.bashrc` so that, if
the user type a command that could exist in the path but doesn't, then the user
is presented with a helpful message about what package to install.

