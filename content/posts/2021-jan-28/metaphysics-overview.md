---
title: "Brief Overview of Thomistic Metaphysics"
date: 2021-01-28T15:20:35-07:00
description: "Outline of the short course for home-school students."
image: "abg-cropped.png"
tags: ["feser", "aquinas", "metaphysics", "course"]
---

This is the outline of my 12-session course (one hour-long session per week)
that briefly introduces the Thomistic flavor of Aristotelian metaphysics to
home-school students, roughly 12 years of age and up.

<!--more-->

## Outline

The text is Edward Feser's [Aquinas: A Beginner's Guide][1].  The aim is to
cover Chapters 1 and 2 on the introduction to metaphysics.  A stretch-goal is
to cover in some detail the first of the Five Ways at the beginning of Chapter
3.

[1]: https://www.amazon.com/Aquinas-Beginners-Guide-Edward-Feser/dp/1851686908

![Aquinas](abg-cropped.png)

I should like to help the student to understand the text.
  - He should read the relevant pages before class and try to comprehend it so
    well as possible.
  - Also, while reading the selection for the week, the student should write
    down every word that he
    - had not seen before,
    - did not know the meaning of, or
    - did not understand the particular usage of.
  - Bring to class
    - the list of words,
    - any questions needing clarification, and
    - any ideas about what was interesting in the reading.

In class, I shall talk about how the material relates to things beyond what the
author discusses.  In the discussion of act and potency, for example, I discuss
the nature of the Devil.  I am happy to discuss whatever the student finds
interesting in the text or in its relationship to other matters.

### Week 1: Introduction (Pages 1--9)

This includes a brief biographical sketch of Aquinas, who died more than 700
years ago, and his encounter with the writings of Aristotle, who had died more
than 1500 years before that (more than 2200 years ago).

The initial reading also includes the introductory paragraphs of Chapter 2, so
that the student gets rough sense of why studying the "metaphysical
presuppositions" is important.  An understanding of metaphysics is necessary
for proper understanding of natural theology, of what can be known about God by
reason alone.  Moreover, the same metaphysical presuppositions, though the
typical scientist seems unaware of them, [are presupposed even by modern
science][2].  This is important because bad philosophy (such as scientism and
materialist reductionism, both associated with atheism) is usually associated
with modern science, and a proper understanding of metaphysics is a shield
against falling into modern philosophical errors.

[2]: https://www.amazon.com/Aristotles-Revenge-Metaphysical-Foundations-Biological/dp/3868382003

### Week 2: Act and Potency (Pages 9--12)

The division of reality into *act* and *potency* underlies (at least
implicitly) all thinking about the physical world.  In any actual physical
thing, which is real, are various potencies or potentialities, each of which is
also *real* even if not actual.  Not every conceivable potentiality is real in
a given actual thing.  The principal job of modern science is to discover
empirically and to predict theoretically the potentialities that are real in
any given actual thing.

The distinction between act and potency allows a coherent understanding of how
anything *changes*.  In fact, Aristotle introduced the distinction precisely to
explain how change happens.  A real potentiality in an actual thing indicates
how the thing might change.  Whenever change occurs, what had been potentially
real becomes actually real.

Note that a potentiality cannot exist on its own.  A potentiality can exist
only *in* something else that is otherwise actual.  So there is a sense in
which potentiality is subordinate to actuality or dependent on actuality.

#### A Digression on Good and Evil

This relationship between potentiality and actuality, the dependence of a
potentiality on an actuality, is reflected in the relationship between *evil*
and *good*.  Feser does not talk about good and evil until much later in the
book, but I think that good and evil (at least as defined by Aquinas) provide a
fun example of a particular kind of asymmetry and dependence.

I am *not* claiming that goodness is a principle of actuality and that evil is
a principle of potentiality, for evil is sometimes quite actual.  Rather, I am
claiming that evil is dependent on good as potentiality is dependent on
actuality (or, as we shall see later, as an accident is dependent on a
substance).

Every evil is subordinate to and depends on a good.  But good does not have
such a dependence on evil.

An *evil* is the absence of what ought to exist in a thing according to its
nature.  Such an absence is a "privation."
- On the one hand, evil can exist as a negative potentiality, a potential
  absence of what ought to exist in something.
- On the other hand, evil can be an actual privation.

Yet evil remains subordinate to the good: An evil cannot exist on its own but
can exist only *in* something else that is otherwise good.  For example, a
glass window can be cracked.  But a window-crack cannot exist on its own; a
window-crack can exist only in a window, just as evil can exist only in
something that is otherwise good.  Yet a window can exist without a crack, and
good can exist without evil.  Good is to evil as a window is to a crack in the
window.

Now, evil is the absence of what *ought* to exist in something.  The word
"ought" here refers to what the thing is.  A three-legged cat is still, in a
sense, a four-legged animal because a cat is a four-legged animal, and a
three-legged cat is still a cat.  But a three-legged cat suffers from an evil,
the absence of a leg.  That absence prevents the three-legged cat from being
fully what it is according to its nature.

In any being other than God, that being could change by way of injury, so that
part of itself, which is potentially absent, becomes actually absent.  This is
evil as a potentiality.

There is no such thing as pure evil, since evil is only a defect in something
else that is otherwise good.  For example, there is great good in Satan;
otherwise he would be powerless. Satan's great evil comes from the fact that a
greatly powerful being has a defect that allows much of his power (which is
good in itself) to remain intact.

### Week 3: Hylemorphism (Pages 13--16)

In every reality except for God, there is both actuality and potentiality. (God
is purely actual.)  The division between the actual and the potential appears
in different ways.

One way in which the division appears is in a physical object, which can be
viewed as a combination of form and matter.  Form is to matter as actuality is
to potentiality.  A piece of wood, for example, could potentially be carved
into many a different form; when the wood takes a particular form, it takes on
a certain actuality. In this case, we call the shape of the wood an *accidental
form* and the wood itself is just ordinary *matter*.

By extension, a *substance* is a combination of *substantial form* and *prime
matter*.  The form represents what the substance actually is, and the prime
matter could potentially be any substance, depending on what form combines with
it to make a substance.

Hylemorphism (approximately "wood-shape" in Greek) is the proposition, that
every physical thing is a combination of matter and form, just as that thing is
a combination of potency and act, respectively.

### Week 4: The Four Causes (Pages 16--23)

The four causes of a thing together provide a complete description of it.
- The *material* cause of a thing is the matter that it is made of. At a
  superficial level this refers to the ordinary matter of common experience.
  But ordinary matter itself, when considered as a *substance*--whether it be
  rubber, water, or anything other substance--also, at bottom, has its own
  material cause called *prime matter*.  We shall see later how the idea of a
  substance is incompatible with materialistic reductionism, a philosophical
  error commonly associated with modern science, but the idea of a substance is
  in fact presupposed by modern science.
- The *formal* cause of a thing is the configuration of the thing's matter. At
  a superficial level this refers to the thing's shape, color, texture, smell,
  etc.  But the formal cause of a substance is called a *substantial form*.
  The substantial form is what gives prime matter the properties of the
  particular substance: the bouncy flexibility of the rubber in a rubber ball
  (if rubber be regarded as a substance), etc.
- The *efficient* cause of a thing made what was a potential reality (before
  the thing existed) become an actual reality (when the thing first came into
  being).  For example, the air in a room at a comfortable temperature and a
  glass sitting on a table in that room are the primary efficient causes of the
  melting of ice in the glass.  The air convects heat to the ice, and the glass
  conducts heat to the ice.  The liquid water that results was a real potential
  in the ice, but the liquid became actual when the glass and the air heated
  the ice.
- The *final* cause of a thing is the purpose for which the thing came into
  being or an end toward which the thing tends.  For example, a match, when
  struck, tends to produce a flame (rather than frost, the sound of a harp, or
  many an unrelated thing).  So producing a flame would be a final cause of a
  match, even if nobody ever intended a match to produce flame.  As it happens,
  though, producing a flame is a final cause of a match also because the match
  is engineered specifically for the purpose of producing flame.

There are also some principles that relate to these causes.
- The *principle of causality* is, most generally, that whatever comes into
  being (whatever is contingent) is caused to be.  With respect to change, this
  principle is that whenever a potency becomes actual, the potency is
  actualized by something else already actual.  This is the meaning of the
  formula, "Whatever moves is moved by another."  (In this case, "moves" and
  "moved" are, respectively, synonymous with "changes" and "changed.")
- The *principle of proportionate causality* is that an effect is proportionate
  to its causes and principles: "whatever perfection exists in an effect must
  be found in the effective cause."  (Note that in modern language, every cause
  has an effect, but this is strictly true only of every *efficient* cause.
  The *effect* is the result of the *efficient* cause; "effect" and "efficient"
  have similar spelling not merely by chance.)  The effect is contained in its
  cause *formally* or *virtually*: If formally, then the effect is already
  actual in some sense in the cause and can be seen there to have the same form
  in the effect, as when a lit candle causes another candle to become lit. If
  virtually, then the effect exists in the cause as a potency, as when a poison
  in one animal causes a sting in another.
- The *principle of finality* is that "every agent acts for an end."  Every
  efficient cause points toward certain effects and not toward others.  The
  effect toward which an efficient cause points is the final cause of that
  efficient cause.

### Week 5: Essence and Existence (Pages 24--31)

To distinguish between a thing's essence and its act of existence is to move
beyond Aristotle into what is a development of Aquinas.

The essence of a thing is the collection of the thing's *primary* features.
This collection allows the thing to be intelligible as the kind of thing that
it is.  A secondary feature might be necessary for the thing to be what it is,
but even a necessary feature is secondary, thus not essential, if it be derived
as a logical consequence from another feature.  (A derived, necessary feature
is a *property*.)

The essence of a thing exists in the thing and also in the intellect that
grasps what the thing is.  In the intellect, the essence has the universal
accidents of species, genus, and specific difference, which are abstracted from
every individual thing.  These accidents also exist in each thing, but not in a
way that is abstracted from the individual thing's characteristics (whether the
characteristic be unique or shared with other individuals of its kind).

For a physical thing, its essence is not merely its form because the intellect
regards the thing as a composite of matter and form; that composition of matter
and form is part of the thing's essence.  Because matter is part of a physical
things essence, it is common to every thing of its kind, but matter is also the
principle by which one individual thing can be distinguished from another.
Here we have used "matter" in two different senses: First, in a sense that is
common to every physical thing; second, in a sense in which one parcel of
matter is designated to one individual thing and another parcel, to another
individual.  Thus we distinguish between "common matter" and "designated
matter."

Because an angel is not a physical thing, there is no designated matter that
could distinguish one individual angel from another of its same species.
Therefore, every angel must be of a different species.  Also, because an angel
is not a composite of form and matter, the angel's essence is just its form.

Regardless of whether a thing's essence involve matter or not, the essence is
distinct from the thing's act of existence.  The created thing is contingent,
which means that it might not have existed.  Just as matter is the potential
principle actualized by the injection of form, so too a thing's essence (which
can involve both matter and form) is the potential principle actualized by
God's creating it (God's holding it in being) while it exists.  Apart from God,
everything that exists is actual, in that it is existing, and potential, in
that it might or might not exist.  Any thing whose essence is something other
than to exist must have existence joined with its essence; God alone has within
His very essence to exist.

### Week 6: The Transcendentals (Pages 31--36)

Being is not a genus.  Every genus is an abstraction that ignores some aspects
of any individual being to which the genus applies.  Moreover, every reality
has being as part of its essence.  When "genus" is applied to each individual
or species within the scope of the genus, "genus" is applied univocally.  But,
for any pair of individuals, when "being" is applied to each, "being" is
applied analogously.

Being is transcendental.  It is convertible with any of thing (res), one
(unum), something (aliquid), good (bonum), and true (verum).  As being pertains
to a thing considered in itself, it is considered positively (as thing) or
negatively (as individual, not divisible, one).  As being pertains to a thing's
relation to other things, the thing is either different from the others (as
something and not something else) or conformal to others.  So far as being
pertains to conformance, it can conform to cognition (as true, so far as the
thing truly represents its species) or to appetite (as good, so far as the
thing ought to exist as the end pointed to by its causes).

The convertibility of being with good means that an evil is not a being but a
*privation*, the absence of what ought to be.

### Week 7: Final Causality, Part I (Pages 36--44)

The principle of finality is that every agent (efficient cause) acts toward
some end.  This is true of the non-living agent, of the living but non-rational
agent, and of the rational agent.  Part of being an agent is to be directed
toward some end, as a match is directed toward making heat when struck.  As it
turns out, everything in Aristotelian metaphysics, even the distinction between
act and potency, is entailed by the principle of finality, for a potential
points toward its becoming actual.

Yet many an advocate of modern philosophy denies the principle of finality
because of one or another misunderstanding, such as that the directedness of a
final cause requires the agent to be conscious of the direction or that the
agent, like the eye of an animal, must show some evidence of design toward its
effect.

One kind of denial from the modern is the claim that the principle of finality
provides too little, as in Moliere's joke that the Aristotelian might observe
that opium has the power to cause drowsiness.  However, while not invoking a
specific checmical cause, the Aristotelian does not deny a chemical cause in
opium; further, the Aristotelian's observation is actually a natural scientific
theory (though a very rough one) following from a scientific fact: The use of
opium is observed to cause drowsiness in the user.

It is true that some of the physical examples of Aristotle and Aquinas are poor
examples in light of modern science, but the fact that the examples are poor
does not mean that the principles exemplified are wrong.  Examples consistent
with modern science exemplify the same metaphysical principles that Aristotle
and Aquinas intended to elucidate, even if some of the physical principles that
Aristotle and Aquinas chose for the examples were wrong.

Some of the rejection of the principle of finality was even early on due to the
superiority of the mechanical method, which looked for specific causes (such as
the chemical causes of opium's dormitive power), in advancing technology, as
compared with the scholastic method, which was not concerned with the
advancement of technology.  Finding a new method good for technology does not
mean that old methods were not good for other things.  There is more to life
than technology.  A mathematical model that enables the building of useful
devices on the supposition that the electron actually exists does not tell us
that the electron actually exists.  The scholastic method of Aquinas is
explicitly concerned about what actually exists (and what exists in potency).
Although modern science is not explicitly interested in things like what exists
actually (and modern philosophers make the mistake of thinking that science
does tell us this), the metaphysical approach of Aristotle and Aquinas *is*
explicitly interested in allowing us to approach the actual with the mind.

Some of the rejection of the principle of finality most recently arises from
those who think that modern science explains everything.  Why should we retain
the scholastic principle of finality, if we don't need it?  The idea that
modern science explains everything that is real is sometimes called
*scientism*.  Yet the mechanical philosophy associated with modern science has
given rise to many seemingly intractable philosophical problems (Descartes'
mind-body problem, the problem of induction, etc.), which are not even problems
at all in terms of Arostotelian metaphysics.  This alone should give one pause
when considering whether modern science can explain everything.

### Week 8: Final Causality, Part II (Pages 44--51)

Although Darwinian theory has been supposed by some to have eliminated any need
for teleology from a scientific understanding, Darwinian theory has from its
beginning used teleological concepts.  For example, the idea that eye is *for*
seeing, or the heart, *for* pumping blood.  After Darwin, those who have wanted
to eliminate teleology have either tried to reduce teleology to efficient
causality or tried to eliminate teleology altogether.  Any such approach has
problems even being coherent (as can be seen above by the fact that an
efficient cause's effect is the efficient cause's final cause).  The discovery
of DNA, which seems to store information that codes *for* certain proteins at
the microscopic level and *for* traits at the macroscopic level, poses a deeper
and more recent problem for those who would eliminate teleology, the intrinsic
directedness of one thing toward another.

Although many who think about modern science imagine that its product is the
identification of "laws of nature," science seems rather to be about finding
out what the hidden natures and properties of individual things are.  A
carefully controlled experiment removes interfering factors in order to isolate
a thing's response to a carefully arranged stimulus.  Yet how a thing will
respond to a stimulus is just an aspect of how that thing is by its nature
*directed toward* a particular effect when the stimulus and the thing itself be
viewed as the causes of the effect.

### Week 9: Efficient Causality (Pages 51--55)

The principle of finality links an efficient cause to its effect, which is the
final cause of the efficient cause.  But there are principles that operate
merely within the realm of efficient causes themselves.

The *principle of proportionate causality*, briefly mentioned above, is a key
principle in scientific reasoning.  Every effect must be contained, either
formally or virtually, within its (efficient) causes.  When one sees a rough
patch in the otherwise smooth, wet sand exposed perhaps a couple of minutes
before by the recession of sea-water from the beach, one concludes that
something other than the motion of the water produced the rough patch.
Perhaps, when one was not looking, an animal dug in that patch right after the
water receded.  Anyway, one knows what effects water can have on sand as waves
crash into the beach and then as the water recedes.  Seeing an effect that
water has no power to cause requires the inference of another cause, such as,
in this case, an animal.

The *principle of causality* is that every contingent thing, every thing that
might or might not exist, has one or more efficient causes.  Hume rejected this
principle, but *coherent* rejection seems impossible.  If something were
suddenly to pop into existence, and if one were to witness this, one would
suspect a trick and would try to find out where the thing came from; one would
look for a transporting cause.  Even if one eventually came to regard as
possible the popping into existence, one would still naturally posit a creative
cause.

### Week 10: Being (Pages 55--61)

### Week 11: The First Way, Part I (Pages 62--73)

### Week 12: The First Way, Part II (Pages 74--81)

