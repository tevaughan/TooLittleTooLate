---
title: "New Debian Packages"
date: 2021-01-18T12:23:43-07:00
description: "New packages of interest to me."
image: ""
tags:
  - "debian"
  - "package"
  - "offlineimap3"
---

- `offlineimap3`

<!--more-->

## offlineimap3

*IMAP/Maildir synchronization and reader support*

OfflineIMAP is a tool to simplify your e-mail reading.  With OfflineIMAP, you
can:

* Read the same mailbox from multiple computers, and have your changes
  (deletions, etc.) be automatically reflected on all computers

* Use various mail clients to read a single mail box

* Read mail while offline (on a laptop) and have all changes synchronized when
  you get connected again

* Read IMAP mail with mail readers that do not support IMAP

* Use SSL (secure connections) to read IMAP mail even if your reader doesn't
  support SSL

* Synchronize your mail using a completely safe and fault-tolerant algorithm.

* Customize which mailboxes to synchronize with regular expressions or lists.

* Synchronize your mail two to four times faster than with other tools or other
  mail readers' internal IMAP support.

In short, OfflineIMAP is a tool to let you read mail how YOU want to.

This is the Python3 port of offlineimap.

Homepage: https://github.com/OfflineIMAP/offlineimap3
