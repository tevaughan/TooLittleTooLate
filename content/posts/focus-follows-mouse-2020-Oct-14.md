---
title: "Focus Follows Mouse"
date: 2020-10-14T12:48:24-06:00
description: "I have enabled mouse-following focus in Windows 10."
image: ""
tags: ["focus", "mouse", "windows-10"]
---

Combining a couple of the comments on [this article at superuser][supart], I
have made it so that the focus follows the mouse after I log in.

[supart]: https://superuser.com/questions/954021/how-do-you-enable-focus-follows-mouse-in-windows-10

<b><i>Script for PowerShell:</i></b>
```
$signature = @"
[DllImport("user32.dll")] public static extern bool SystemParametersInfo(int uAction, int uParam, int pvParam, int flags);
"@

$systemParamInfo = Add-Type -memberDefinition  $signature -Name SloppyFocusMouse -passThru

[Int32]$newVal = 1

$systemParamInfo::SystemParametersInfo(0x1001, 0, $newVal, 2)
```

NOTE: Run the same script but with `$newVal = 0` in order to restore
click-to-focus.

<!--more-->

## Details

The tricky bit was to understand
  - that the answer by [golvok][golvok] at 2017 May 15 20:53 is the right way
    to go but slightly wrong and
  - that the answer by [Matthijs][Matthijs] at 2019 May 13 19:07 allows the
    proper correction to golvok.

[golvok]: https://superuser.com/users/402639/golvok
[Matthijs]: https://superuser.com/users/989144/matthijs

My script is almost the same as golvok's but does not pase `$newVal` by
reference because that is a bug in golvok's script.

### Location of Script

I put my script into the file
```
C:\Users\<UserName>\<SomePath>\focus-follows-mouse.ps1
```

*NOTE:* It is important that `<SomePath>` have no spaces. I found that running
the script by way of a shortcut with the "Target:"-field below did not work
when `<SomePath>` had spaces.

### How To Run Script at Login

In the folder
```
C:\Users\<UserName>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
```
I itially made a shortcut whose Target-field contains
```
%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe -executionpolicy bypass -File C:\Users\<UserName>\<SomePath>\focus-follows-mouse.ps1
```
My current shortcut eliminates `-executionpolicy bypass -File` but has what
appears earlier and later on the line above.
