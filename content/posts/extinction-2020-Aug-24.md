---
title: "Extinction Near Sunrise and Sunset"
date: 2020-08-24T10:09:16-06:00
description: >-
  Ordinary extinction by Rayleigh scattering makes the sun appear red at
  sunrise and sunset.  If present, airborne particles from a forest-fire nearby
  cause extra extinction, which further reddens the appearance of the sun's
  disk, but less scattering of blue light makes the sky grayer.
image: "extinction-2020-Aug-24/orange-sun-cropped.jpg"
tags: ["scattering", "extinction", "sunset", "sunrise"]
---

I took a picture of the sun while I was out for my exercise in the morning.
The time was around 07:00.  Because the picture was taken in summer, the sun
was already high enough so that it would ordinarily not be red as it appears in
the image.

<!--more-->

<a href="/posts/extinction-2020-Aug-24/orange-sun.jpg">
  <img src="/posts/extinction-2020-Aug-24/orange-sun.jpg"/>
</a>

"Extinction" refers to the diminution of the intensity of a ray of light as it
passes through a medium, such as the atmosphere.  Extinction has two principal
causes:

1. *absorption* and
2. *scattering*.

On the one hand, light might be absorbed by the medium (or by particles in the
medium).  On the other hand, light in the beam might be scattered out of the
beam into random directions, typically by particles in the medium or by defects
in the medium (like a smoke-particle in air or an air-bubble in water).

The ordinary redness of the sun near sunset is due mostly to scattering of the
blue light out of every beam of light beginning at the surface of the sun and
ending at the eye.  That blue light is not absorbed by the earth's atmosphere
but scattered in random directions into the rest of the sky, where it scatters
again and again.  This makes the daytime sky blue.  So the sky is blue because
the sunset and sunrise are red.

The extraordinary redness of the sun when there are particles of soot in the
atmosphere indicates that many of the particles are smaller than the reddest
wavelengths of light from the sun.  But the Mie-scattering of the bluer light
off of soot-particles is perhaps not so efficient as Rayleigh scattering from
atmospheric molecules, and it seems that the soot-particles absorb some of the
bluer light.  So if enough soot be in the sky, then it does not appear blue but
gray.  The particles from a forest-fire seem to have a gray-scattering effect
like that of the Mie-scattering from atmospheric water-droplets in ordinary
clouds, but, unlike an ordinary cloud, a cloud of soot lets red light pass
through better than blue light.

The following plot is from a neat [page at Pennsylvania State University][1].

<a href="/posts/extinction-2020-Aug-24/scattering.jpg">
  <img src="/posts/extinction-2020-Aug-24/scattering.jpg"/>
</a>

Visible light corresponds to the narrow, vertical band from the left edge of
the plot to the first tick on the horizontal axis.  One sees that dust, smoke,
and haze consists of particles with sizes right in the range of the wavelengths
in visible light.  (Credit for the image goes to W. Brune and Grant Petty.)

[1]: https://www.e-education.psu.edu/meteo300/node/785

