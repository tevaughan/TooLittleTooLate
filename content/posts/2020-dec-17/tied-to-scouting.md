---
title: "Tied to Scouting"
date: 2020-12-17T17:14:58-07:00
description: "On the occasion of my last formal event in BSA."
image: ""
tags: ["BSA", "court"]
---

I have written down a few words for a brief speech at the final court of honor
for Troop 565 in Longmont.

<!--more-->

As I consider the final court of honor for Longmont's Troop 565, I am reminded
that I shall be forever tied to scouting.  What tie me are

- friendships with those whom I have met while I participated as an adult
  leader,

- memories that I can, so long as I live with a sound mind, share with my
  boys, now Eagle Scouts, and

- skills that I have learned by participating in scouting.

As to the friendships, I think especially of Dan Woods, Jim Cramer, Randy Opp,
and Pete Zolkos, but there are many others, too, of fond memory.

I have spent many days working with Dan Woods in helping to fix up his cabin
and enjoying its seclusion. There we have talked about philosophy, politics,
mathematics, and what we might do for the troop.  Dan opened his cabin for the
troop to come for camping and nearby activities like fishing and hiking.  Many
of us spent late nights talking about Star Trek around the fire at various
campsites, including the site right next to the cabin.

Jim Cramer, our long-time committee-chair, has opened his house on many an
occasion.  Regularly, we had committee-meetings in his dining room.  He
provided snacks, and each attendee usually brought drinks. We might have spent
more time talking about random tangents than in doing business for the troop,
but we did a good deal of both.  Jim also opened his house for projects,
especially for any project involving work in his wood-shop in the basement.

Randy Opp would often step up to help when everyone else (including me) failed
to do so.  Taking over administration of the troop's funds is perhaps just a
relatively recent example. But he was almost always willing to help out however
he could, with his truck, with his presence to help with two-deep leadership,
and with his moral support.  We have had many great conversations about every
subject under the Sun.  We both share a love for G. K. Chesterton.

I haven't seen Pete Zolkos much recently, since his son finished his scouting
career, but, like Randy, Pete was a great friend to have around the campfire in
the evening. And he would reliably show up in times of need with intelligence
and skill.

Activities in scouting did not always involve pleasure in the moment, but they
always did allow me to spend time with one or more of my boys.  I recall how
frustrated Nicholas was in trying to discharge his responsibilities as senior
patrol leader.  I recall how Patrick was frustrated with constraints on
preparations for camping.  I recall how Ethan suffered hypothermia at Klondike.
But each of these memories is still a memory of my presence in the life of a
son.  And there are many others besides, both good and bad.

As to skills that I shall take with me, I provide a memory and a concrete
example.  Not long after I joined the troop, I participated in
scoutmaster-training.  Two of the things that I'll never forget are, from that
training, how to tie a double half-hitch and how to tie a taut-line hitch.
There are many useful knots, but these two naturally go together, often the
first at one end of a string and the second at the other end.  But these two I
remember because I have ever since used them with frequence.  Just this
morning, in fact, while I was trying to make space on my desk, where I work
from home, I decided to mount some cluttered computer-related objects
underneath the desk.  The only materials that I could find handy for this task
were some small eye-hook screws and some kite-string.  By fastening the hooks
to either side of each object that I wanted to fasten to the underside of the
desk, I was able to use a double half-hitch to secure one end of a length of
string to one hook and then a taut-line hitch to secure the other end of the
string to another hook.  Then I adjusted the tautness of the string until it
just fit the object that I wanted to suspend. In this way, I mounted my docking
station and the power supply for my laptop.  Just this morning, I used skills
learned in scouting to clean up my space.

In the end, I shall be forever tied to scouting, by friends that I have made,
by memories that I can share with my boys, and by skills that, learned in
scouting, I continue profitably to employ.

