---
title: "New Debian Packages for 2020 Oct 25"
date: 2020-10-25T14:28:25-06:00
description: "Today's new packages of interest in sid."
image: ""
tags:
  - "6502"
  - "dasm"
  - "debian"
  - "package"
---

New Debian-packages for today include
- dasm

Of interest is that dasm is an assembler that can be used for 6502.

<!--more-->

## dasm

DASM is a versatile macro assembler with support for several 8-bit
microprocessors including MOS 6502 & 6507; Motorola 6803, 68705, and
68HC11; Hitachi HD6303 (extended Motorola 6801) and Fairchild F8.

DASM boasts a number of features including:

* fast assembly
* supports several common 8 bit processor models
* takes as many passes as needed
* automatic checksum generation, special symbol ??...??
* several binary output formats available
* allows reverse indexed origins
* multiple segments, BSS segments (no generation), relocatable origin
* expressions, as in C
* 32-bit integer expressions
* no real limitation on label size
* complex pseudo-ops, repeat loops, macros

Homepage: https://dasm-assembler.github.io/
