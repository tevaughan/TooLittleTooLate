---
title: "New Debian Packages for 2020 Nov 04"
date: 2020-11-04T17:00:12-07:00
description: "Today's new packages of interest in sid."
image: ""
tags:
  - "case"
  - "crystal-facet"
  - "debian"
  - "jupyter"
  - "k-d-tree"
  - "libatomic-queue"
  - "libnanoflann"
  - "package"
  - "uml"
  - "xeus"
---

New Debian-packages for today include
- `crystal-facet-uml`
- `libatomic-queue-dev`
- `libnanoflann-dev`
- `xeus-dev`

<!--more-->

## crystal-facet-uml

*Diagram-documentation tool for system and software architecture*

`crystal_facet_uml` creates sysml/uml diagrams to document system and software
architecture.

As software architect, you create a set of diagrams describing use-cases,
requirements, structural views, behavioral and deployment views.
`crystal_facet_uml` keeps element names and element hierarchies consistent. It
exports diagrams in svg, pdf, ps and png formats to be used in text processing
systems like DocBook, html, LaTeX.

Homepage: http://andreaswarnke.de/crystal_facet_uml/html/index.html

## libatomic-queue-dev

*Devel-files for C++ atomic_queue library*

C++11 multiple-producer-multiple-consumer lockless queues based on circular
buffer with std::atomic.  The main design principle these queues follow is
simplicity: the bare minimum of atomic operations, fixed size buffer, value
semantics.

The circular buffer side-steps the memory reclamation problem inherent in
linked-list based queues for the price of fixed buffer size. See *Effective
memory reclamation for lock-free data structures in C++* for more details.

These qualities are also limitations:

* The maximum queue size must be set at compile time or construction time.
* There are no OS-blocking push/pop functions.

Nevertheless, ultra-low-latency applications need just that and nothing more.
The simplicity pays off, see the throughput and latency benchmarks.

Available containers are:

* `AtomicQueue` - a fixed size ring-buffer for atomic elements.
* `OptimistAtomicQueue` - a faster fixed size ring-buffer for atomic elements
  which busy-waits when empty or full.
* `AtomicQueue2` - a fixed size ring-buffer for non-atomic elements.
* `OptimistAtomicQueue2` - a faster fixed size ring-buffer for non-atomic
  elements which busy-waits when empty or full.

These containers have corresponding `AtomicQueueB`, `OptimistAtomicQueueB`,
`AtomicQueueB2`, `OptimistAtomicQueueB2` versions where the buffer size is
specified as an argument to the constructor.

This package contains the header files and static library.

Homepage: https://github.com/max0x7ba/atomic_queue

## libnanoflann-dev

*C++11 header-only library for Nearest Neighbor Search with KD-Trees*

Nanoflann is a fork of the FLANN library. Originally born as a child project of
the Mobile Robot Programming Toolkit, nanoflann is now available as stand-alone
library.

It trades some of the flexibility of FLANN for raw execution speed and is much
more memory efficient, as it avoids unnecessary copies of point cloud data.

Homepage: https://github.com/jlblancoc/nanoflann

## xeus-dev

*C++ Implementation of the Jupyter Kernel protocol (headers)*

xeus enables custom kernel authors to implement Jupyter kernels more easily. It
takes the burden of implementing the Jupyter Kernel protocol so developers can
focus on implementing the interpreter part of the Kernel.

This package contains the headers and cmake config.

Homepage: https://github.com/jupyter-xeus/xeus

