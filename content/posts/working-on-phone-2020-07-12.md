---
title: Post Made on My Phone
date: 2020-07-12
tags: ["blog", "phone", "pain"]
---

Yes, I cloned the blog onto my phone, edited a new post, and pushed it back,
but I doubt that I'll be doing that often, or ever again.

<!--more-->

Using the app called `WorkingCopy`, I connected to gitlab and cloned the repo
for my blog.  Also I made a new file for this post.

Although I could have edited the file in WorkingCopy, paid for an editing-app:
Using the app called `Editorial`, I am typing the source for this post.

This is such a pain in the butt, though, as to make me think that I won't be
doing much editing on my phone, at least not without a bluetooth keyboard
connected.
