---
title: "Power-Law and Death"
date: 2020-08-22T12:50:09-06:00
description: "Hearing a comment by Neil Johnson prompted me to do some math."
image: "power-2020-Aug-22/stdev.png"
tags: ["power-law", "mindscape", "complexity", "probability", "statistics"]
---

This morning, while jogging, I listened to [Sean Carroll's Mindscape
Podcast][1].  The guest was Neil Johnson, who talked about complexity.
"Complexity" not as in "computational complexity" but as in "the complexity of
a system."  Johnson made a mathematical claim that caught me by surprise and
prompted me to write a short article about it.  After working through the
mathematics, I find that his claim is not quite what it seemed to be, but the
claim was not wrong.

[1]: https://www.preposterousuniverse.com/podcast/

<!--more-->

## The Claim

One interesting thing about a complex system is that it is likely described by
a power-law, a function in whose expression the independent variable is raised
to some constant power.  The power-law indicates the distribution of
probability over some property held by pieces of the system.  For example, an
economy is a complex system, and, over some range of yearly incomes, their
distribution seems roughly to be a power-law.

What caught my attention was a claim made by Johnson while he was discussing
another example, the number of deaths per battle in a war.  He mentioned that
the best fitting exponent (or power, or even "slope" in Johnson's terminology)
across a number of different conflicts, when one side is much less powerful
than the other, is almost always around 2.5.  When Carroll asked, Why 2.5?,
Johnson did not have a neat answer, but he made a fascinating comment, the one
that prompted me to write this short article.

Johnson said that when the power of a power-law distribution lies between two
and three, the standard deviation diverges to infinity.  He indicated that this
is indicative of a system in which a kind of maximum agility or flexibility
exists.  When I heard that there is something odd about powers between two and
three, I decided to take a look for myself at the mathematics.

## What I Found

So far as the standard deviation is concerned it is simply not defined for
powers less than or equal to three.  From that point of view, there is nothing
special about the range between two and three.  Johnson's comment seemed to
suggest that the standard deviation might be defined for powers smaller than
two and greater than three, but the standard deviation is undefined for powers
smaller than two as well.

However, the mean of the distribution is defined for powers larger than two.
Thus, for powers between two and three, one could at least write down the
integral for the variance, which is expressed in terms of the mean.  That
integral will diverge for powers between two and three, but there is a sense in
which one cannot even write down the integral for powers less than or equal to
two, for not even the mean would exist.

Johnson's claim makes the most sense if one consider that, between powers two
and three, the mean converges but not the standard deviation.  Perhaps he was
suggesting that the existence of a mean value is what makes the distribution
applicable to the complex system in some sense and that the divergence of the
standard deviation indicates something about the unpredictable nature of the
actors in the system.

## Mathematical Details

After hearing Johnson's comment, I immediately wanted to do a bit of
mathematical analysis of the power-law distribution.  Let \\(\phi\\) be a
power-law distribution expressed as

$$\phi_a(x) = C_a x^{-a},$$

where \\(a\\) is some positive constant, and \\(C_a\\) is the corresponding
normalization-coefficient. We consider only \\(x \geq 1\\).  The area under the
tail of \\(\phi_a\\) is finite only if \\(a > 1\\).

### Normalization

Let us consider the power-law as a distribution over the domain
\\([1,\infty)\\).  Then we can solve for \\(C_a\\) under the condition that
\\(\phi_a\\) is normalized.  This requires that \\(a > 1\\) because the tail
must have finite area in order for the distribution to be normalizable.

$$
\begin{aligned}
\int_1^{\infty} \phi_a(x) \\; \mathrm{d}x &= 1\\\\ 
C_a \int_1^{\infty} x^{-a} \\; \mathrm{d}x &= 1\\\\ 
\frac{C_a}{a - 1} &= 1
\end{aligned}
$$

So the normalization-coefficient is \\(C_a = a - 1\\), and we have, for the
form of the distribution,
$$
\phi_a(x) = [a - 1] \\; x^{-a}.
$$

<a href="power-2020-Aug-22/power.png">
<img src="power-2020-Aug-22/power.png" alt="phi"/>
</a>

### Mean

The mean \\(\mu\\) of a distribution is its first moment \\(\mu = \langle x
\rangle\\).  For the power-law, we have
$$
\begin{aligned}
  \mu_a &=         \int_1^{\infty} x \\; \phi_a(x) \\; \mathrm{d}x\\\\ 
        &= [a - 1] \int_1^{\infty} x^{-[a - 1]}    \\; \mathrm{d}x\\\\ 
  \mu_a &= \frac{a - 1}{a - 2}.
\end{aligned}
$$
The mean is defined only for \\(a > 2\\).  Otherwise, the integral diverges.
Although the power-law distribution is normalizable for any \\(a > 1\\), the
distribution has no mean for \\(1 < a \leq 2\\).

The requirement that \\(a > 2\\) for the mean of the distribution to be defined
is equivalent to the assertion that if an infinitely long sheet of metal with
constant mass-density were shaped so that its width at position \\(x\\) along
its length were given by \\(\phi_a(x)\\), and if \\(1 < a \leq 2\\), then the
sheet would have finite mass (even though it is infinitely long), but its
center of mass would be infinitely far from the wide end.

<a href="power-2020-Aug-22/mean.png">
<img src="power-2020-Aug-22/mean.png" alt="mu"/>
</a>

### Standard Deviation

The variance involves the second moment.
$$
\begin{aligned}
  \langle x^2 \rangle_a
    &=         \int_1^{\infty} x^2 \\; \phi_a(x) \\; \mathrm{d}x\\\\ 
    &= [a - 1] \int_1^{\infty} x^{-[a - 2]}      \\; \mathrm{d}x\\\\ 
  \lang x^2 \rangle_a
    &= \frac{a - 1}{a - 3}
\end{aligned}
$$

For the variance we have
$$
\begin{aligned}
  \sigma_a^2
    &= \int_1^{\infty} \left[x - \mu_a\right]^2 \phi_a(x) \\; \mathrm{d}x\\\\ 
    &= \langle x^2 \rangle_a - 2\mu_a\langle x \rangle_a + \mu_a^2\\\\ 
    &= \langle x^2 \rangle_a - \mu_a^2\\\\ 
    &= \frac{a - 1}{a - 3} - \left[\frac{a - 1}{a - 2}\right]^2\\\\ 
    &= \frac{a - 1}{[a - 3][a - 2]^2}\left[[a - 2]^2 - [a - 1][a - 3]\right]\\\\ 
  \sigma_a^2
    &= \frac{[a - 1][2a + 1]}{[a - 3][a - 2]^2},
\end{aligned}
$$
and so the standard deviation is
$$
\sigma_a = \sqrt{\frac{[a - 1][2a + 1]}{[a - 3][a - 2]^2}}.
$$

The standard deviation is defined only for \\(a > 3\\).  Otherwise, the
integral for the variance diverges.

<a href="power-2020-Aug-22/stdev.png">
<img src="power-2020-Aug-22/stdev.png" alt="sigma"/>
</a>

### Conclusion

- The power-law distribution is normalizable for any \\(a > 1\\);
- the mean converges for any \\(a > 2 \\); and
- the standard deviation converges for any \\(a > 3\\).

